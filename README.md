# lustre_routed
Lustre routed is a helper library for [Lustre](https://lustre.build/)
that helps create single-page-applications with the JS history routing
API.  It allows you to create applications with different routes without
having a full-page refresh between the routes.

[![Package Version](https://img.shields.io/hexpm/v/lustre_routed)](https://hex.pm/packages/lustre_routed)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/lustre_routed/)

## Quickstart
```sh
gleam add lustre_routed
```
Take a look at the [basic example](https://gitlab.com/kgroat/lustre_routed/-/tree/main/examples/basic) to get an idea of how to use the library.

Further documentation can be found at <https://hexdocs.pm/lustre_routed>.
