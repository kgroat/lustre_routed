#!/usr/bin/env bash
mkdir -p build/.lustre/static
cp priv/static/* build/.lustre/static

# TODO: provide --spa flag when it is supported
# https://github.com/lustre-labs/lustre/commit/80b418f6ee44321d55ed18375d34db1d35620deb
gleam run -m lustre dev
