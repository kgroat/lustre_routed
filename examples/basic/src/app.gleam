import gleam/dynamic
import gleam/int
import gleam/io
import gleam/list
import gleam/option.{type Option, None, Some}
import lustre/attribute
import lustre/effect
import lustre/element
import lustre/element/html
import lustre_http
import lustre_routed.{perform}
import lustre_routed/link.{link}
import app/api_helpers

pub fn main() {
  lustre_routed.routed_application(init, update, loader, view)
}

pub type State {
  State(greeting: String, name: Option(String))
}

pub type Action {
  SetName(String)
  SetGreeting(String)
  Noop
}

fn init(_) {
  #(State(greeting: "Hello", name: None), lustre_routed.listen_for_navigation())
}

fn update(state: State, action: Action) {
  case action {
    SetName(name) -> #(State(..state, name: Some(name)), effect.none())
    SetGreeting(greeting) -> #(
      State(..state, greeting: greeting),
      effect.none(),
    )
    Noop -> #(state, effect.none())
  }
}

fn loader(route: List(String), _state: State) {
  case route {
    ["greet", "random"] -> effect.batch([fetch_name(), fetch_greeting()])
    _ -> effect.none()
  }
}

fn view(route: List(String), state: State) {
  let content = case route {
    [] -> element.text("Welcome Home")
    ["greet", "random"] -> {
      case state.name {
        Some(name) -> element.text(state.greeting <> ", " <> name)
        None -> element.text("Loading...")
      }
    }
    ["greet", name] -> element.text("Hello, " <> name)
    _ -> element.text("404")
  }

  html.div([], [
    html.nav(
      [
        attribute.style([
          #("display", "flex"),
          #("gap", "16px"),
          #("background", "#ccc"),
          #("padding", "8px 16px"),
        ]),
      ],
      [
        link("/", [], [element.text("Home")]),
        link("/greet/world", [], [element.text("Hello")]),
        link("/greet/random", [], [element.text("Random")]),
      ],
    ),
    html.main([attribute.style([#("padding", "8px 0")])], [content]),
  ])
}

fn fetch_and_select(path: String, action: fn(String) -> Action) {
  let decoder = dynamic.list(dynamic.string)

  lustre_http.get(
    api_helpers.get_root() <> path,
    lustre_http.expect_json(decoder, fn(res) {
      case res {
        Ok(items) -> {
          let idx = int.random(list.length(items))
          let assert Ok(item) = list.at(items, idx)
          perform(action(item))
        }
        Error(err) -> {
          io.println_error("Failed to fetch " <> path)
          io.debug(err)
          perform(Noop)
        }
      }
    }),
  )
}

fn fetch_name() {
  fetch_and_select("/static/names.json", SetName)
}

fn fetch_greeting() {
  fetch_and_select("/static/greetings.json", SetGreeting)
}
