//// Lustre routed is a helper library for [Lustre](https://lustre.build/)
//// that helps create single-page-applications with the JS history routing
//// API.  It allows you to create applications with different routes without
//// having a full-page refresh between the routes.

import lustre.{type App}
import lustre/effect.{type Effect}
import lustre/element.{type Element}
import lustre_routed/internals/router

/// When a route is passed to your view and loader functions it will be
/// a list of strings.  The path is split by its forward slashes, so that
/// "/my/cool/route" gets split to ["my", "cool", "route"].
pub type Route =
  List(String)

/// The internal action/message type which is used by the routed application.
pub opaque type RoutedAction(orig_action) {
  Perform(orig_action)
  GoTo(path: String)
  Redirect(path: String)
  Back
  Forward
  SetPath(parsed_path: Route)
}

/// The internal state/model which is used by the routed application to
/// track its own route state as well as your own state.
pub opaque type RoutedModel(model) {
  RoutedModel(route: Route, inner_model: model)
}

/// Creates a lustre application which uses history-based routing.
pub fn routed_application(
  init: fn(flags) -> #(model, Effect(RoutedAction(orig_action))),
  update: fn(model, orig_action) -> #(model, Effect(RoutedAction(orig_action))),
  loader: fn(Route, model) -> Effect(RoutedAction(orig_action)),
  view: fn(Route, model) -> Element(RoutedAction(orig_action)),
) -> App(flags, RoutedModel(model), RoutedAction(orig_action)) {
  let new_init = fn(flags: flags) {
    let split_path = router.get_current_path()
    let orig_init = init(flags)

    #(
      RoutedModel(split_path, orig_init.0),
      effect.batch([orig_init.1, loader(split_path, orig_init.0)]),
    )
  }

  let new_update = fn(
    routed_model: RoutedModel(model),
    action: RoutedAction(orig_action),
  ) {
    case action {
      Perform(sub_action) -> {
        let orig_update = update(routed_model.inner_model, sub_action)
        #(
          RoutedModel(..routed_model, inner_model: orig_update.0),
          orig_update.1,
        )
      }
      GoTo(path) -> {
        let split_path = router.parse_path(path)
        case routed_model.route == split_path {
          True -> #(routed_model, effect.none())
          False -> {
            router.push_path(path)
            #(
              RoutedModel(..routed_model, route: split_path),
              loader(split_path, routed_model.inner_model),
            )
          }
        }
      }
      Redirect(path) -> {
        let split_path = router.parse_path(path)
        case routed_model.route == split_path {
          True -> #(routed_model, effect.none())
          False -> {
            router.replace_path(path)
            #(
              RoutedModel(..routed_model, route: split_path),
              loader(split_path, routed_model.inner_model),
            )
          }
        }
      }
      Back -> {
        router.back()
        #(routed_model, effect.none())
      }
      Forward -> {
        router.forward()
        #(routed_model, effect.none())
      }
      SetPath(split_path) -> {
        case routed_model.route == split_path {
          True -> #(routed_model, effect.none())
          False -> #(
            RoutedModel(..routed_model, route: split_path),
            loader(split_path, routed_model.inner_model),
          )
        }
      }
    }
  }

  let new_view = fn(routed_model: RoutedModel(model)) {
    view(routed_model.route, routed_model.inner_model)
  }

  lustre.application(new_init, new_update, new_view)
}

/// Used to dispatch an action to your own internal state.
pub fn perform(action: action) {
  Perform(action)
}

/// Used to push a new route into the browser's history stack.
pub fn go_to(route: String) {
  GoTo(route)
}

/// Used to replace the current route in the browser's history.
pub fn redirect(route: String) {
  Redirect(route)
}

/// Used to push the next route back onto the browser's history stack
/// (equivalent to hitting the forward button in the browser).
pub fn forward() {
  Forward
}

/// Used to pop the latest route off the browser's history stack
/// (equivalent to hitting the back button in the browser).
pub fn back() {
  Back
}

/// Create a lustre effect that listens for changes in the browser's 
/// history when the user hits the forward or backward buttons.
pub fn listen_for_navigation() {
  effect.from(fn(handler: fn(RoutedAction(msg)) -> Nil) -> Nil {
    router.register_callback(fn(split_path) { handler(SetPath(split_path)) })
  })
}
