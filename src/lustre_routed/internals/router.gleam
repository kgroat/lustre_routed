import gleam/list
import gleam/regex
import gleam/result
import gleam/string

pub fn get_current_path() {
  parse_path(priv_get_current_path())
}

pub fn register_callback(cb: fn(List(String)) -> Nil) {
  priv_register_callback(fn(path) { cb(parse_path(path)) })
}

pub fn parse_path(full_path) {
  let assert Ok(path_separator) = regex.from_string("/")
  let assert #(path, _) =
    string.split_once(full_path, "?")
    |> result.unwrap(#(full_path, ""))

  let path_parts =
    regex.split(path_separator, path)
    |> list.filter(fn(part) { string.length(part) > 0 })

  path_parts
}

@external(javascript, "../../router.ffi.mjs", "pushPath")
pub fn push_path(_path: String) -> Nil {
  Nil
}

@external(javascript, "../../router.ffi.mjs", "replacePath")
pub fn replace_path(_path: String) -> Nil {
  Nil
}

@external(javascript, "../../router.ffi.mjs", "goForward")
pub fn forward() -> Nil {
  Nil
}

@external(javascript, "../../router.ffi.mjs", "goBack")
pub fn back() -> Nil {
  Nil
}

@external(javascript, "../../router.ffi.mjs", "getCurrentPath")
fn priv_get_current_path() -> String {
  "/"
}

@external(javascript, "../../router.ffi.mjs", "registerCallback")
fn priv_register_callback(_cb: fn(String) -> Nil) -> Nil {
  Nil
}
