import gleam/dynamic
import gleam/result
import lustre/attribute
import lustre/element/html
import lustre/event
import lustre_routed.{go_to}

/// Creates an `<a>` element which hooks into the history api.  Clicking
/// on the element will not cause a full page refresh but instead will push
/// a new element onto the history stack.
pub fn link(href: String, attributes, content) {
  html.a(
    [
      attribute.href(href),
      event.on("click", fn(ev) {
        use meta_down <- result.try(dynamic.field("metaKey", dynamic.bool)(ev))
        use ctrl_down <- result.try(dynamic.field("ctrlKey", dynamic.bool)(ev))
        use shift_down <- result.try(dynamic.field("shiftKey", dynamic.bool)(ev))

        case meta_down, ctrl_down, shift_down {
          // If no modifiers are down, prevent default and navigate
          False, False, False -> {
            event.prevent_default(ev)
            Ok(go_to(href))
          }
          // Otherwise allow the event to continue
          _, _, _ -> Error([])
        }
      }),
      ..attributes
    ],
    content,
  )
}
