const handlers = new Set()

window.addEventListener('popstate', function (ev) {
  handlers.forEach(cb => cb(location.pathname))
})

export function pushPath(path) {
  if (path === location.pathname) return
  history.pushState(null, '', path)
}

export function replacePath(path) {
  if (path === location.pathname) return
  history.replaceState(null, '', path)
}

export function getCurrentPath() {
  return location.pathname
}

export function registerCallback(cb) {
  handlers.add(cb)
}

export function goForward() {
  history.forward()
}

export function goBack() {
  history.back()
}
